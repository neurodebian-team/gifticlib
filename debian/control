Source: gifticlib
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Michael Hanke <michael.hanke@gmail.com>,
           Yaroslav Halchenko <debian@onerussian.com>,
           Étienne Mollier <emollier@debian.org>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake,
               zlib1g-dev,
               libnifti2-dev,
               libexpat1-dev | libexpat-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/gifticlib
Vcs-Git: https://salsa.debian.org/med-team/gifticlib.git
Homepage: http://www.nitrc.org/projects/gifti
Rules-Requires-Root: no

Package: libgiftiio0
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: IO library for the GIFTI cortical surface data format
 GIFTI is an XML-based file format for cortical surface data. This reference
 IO implementation is developed by the Neuroimaging Informatics Technology
 Initiative (NIfTI).
 .
 This package contains the shared library.

Package: libgiftiio-dev
Architecture: any
Section: libdevel
Depends: libgiftiio0 (= ${binary:Version}),
         libnifti-dev,
         ${shlibs:Depends},
         ${misc:Depends}
Description: IO library for the GIFTI cortical surface data format - headers
 GIFTI is an XML-based file format for cortical surface data. This reference
 IO implementation is developed by the Neuroimaging Informatics Technology
 Initiative (NIfTI).
 .
 This package provides the header files and static library.

Package: gifti-bin
Architecture: any
Section: utils
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: tools shipped with the GIFTI library
 GIFTI is an XML-based file format for cortical surface data. This reference
 IO implementation is developed by the Neuroimaging Informatics Technology
 Initiative (NIfTI).
 .
 This package provides the tools that are shipped with the library
 (gifti_tool and gifti_test).
